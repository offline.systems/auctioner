BINARIES=auctioner
DOCKERFILE=build/docker/alpine.Dockerfile
HOSTS=\
	138.68.244.181\
	165.227.5.51\

help:
	@echo "all:          make all binaries ($(BINARIES))."
	@echo "clean:        delete temporary files and binaries ($(BINARIES))."
	@echo "coverage:     run integration tests and show coverage in browser."
	@echo "deploy:       deploy auctioner to server(s)."
	@echo "docker-clean: stop auctioner container and clean up containers/images/volumes."
	@echo "docker-run:   start auctioner container."
	@echo "docker:       build auctioner docker image."
	@echo "fmt:          format source."
	@echo "generate:     re-generate generated sources."
	@echo "help:         show this help."
	@echo "integration:  execute integration tests."
	@echo "lint:         execute source linters."
	@echo "setup:        install tools needed for linting, formatting, code generation, etc."
	@echo "auctioner:    compile the auctioner binary."
	@echo "test:         execute code tests, excluding integration tests."
	@echo "upgrade:      upgrade dependencies."
	@echo "vendor:       tidy dependencies and vendor them."

all: $(BINARIES)

bootstrap:
	-go run $(PWD)/poc/add-domain -blacklist downloads.monoprice.com
	-go run $(PWD)/poc/add-domain -blacklist facebook.com
	-go run $(PWD)/poc/add-domain -blacklist twitter.com
	-go run $(PWD)/poc/add-domain -blacklist monoprice.com
	-go run $(PWD)/poc/add-domain www.monoprice.com

check-pipeline:
	go install gitlab.com/offline.systems/check-pipeline/cmd/check-pipeline@latest
	check-pipeline -repo "offline.systems/auctioner"

clean:
	rm -f $(BINARIES)
	rm -f *.log
	rm -f coverage.out

coverage: integration
	go tool cover -html=coverage.out

docker: DD_VERSION=$(shell git rev-parse --short HEAD)
docker:
	docker build -t registry.gitlab.com/offline.systems/auctioner --build-arg DD_VERSION=$(DD_VERSION) -f $(DOCKERFILE) .

docker-clean:
	-docker stop auctioner
	-docker stop rabbitmq
	-docker stop mongo
	# docker ps --filter status=exited -q | xargs docker rm
	# docker ps --filter status=created -q | xargs docker rm
	docker volume prune -f

docker-run: docker-services
	-docker stop auctioner
	-docker rm auctioner
	docker run -d \
		--env SLOWPOKER_ARGS="" \
		--name auctioner \
		-p 2222:2222 \
		-p 2323:2323 \
		-p 8080:8080 \
		registry.gitlab.com/offline.systems/offline.systems

docker-services:
	# RabbitMQ
	if [ -z $$(docker ps --format '{{.Names}}' | grep 'rabbitmq') ]; \
		then \
			docker pull rabbitmq:management; \
			docker stop rabbitmq; \
			docker rm rabbitmq; \
			docker run \
				--name rabbitmq \
				--restart=no \
				-p 5672:5672 \
				-d \
				rabbitmq:management; \
		fi
	# MongoDB
	if [ -z $$(docker ps --format '{{.Image}}' | grep 'mongo:latest') ]; \
		then \
			docker pull mongo:latest; \
			docker rm mongo; \
			docker run \
				--name mongo \
				-d \
				-p 27017:27017 \
				mongo:latest; \
		fi
	# Reset the DB.
	docker exec -t mongo sh -c 'mongosh auctioner --eval "db.dropDatabase()" >/dev/null'
	docker exec -i mongo sh -c 'mongosh auctioner' < ./pkg/store/mongo/indexes.js > /dev/null

ifeq ($(DEBUG),true)
deploy: ARGS="-debug"
endif
deploy: SHA=$(shell git rev-parse HEAD)
deploy: $(HOSTS:=/deploy)

$(HOSTS:=/deploy): HOST=$(@D)
$(HOSTS:=/deploy): check-pipeline
	ssh $(HOST) "docker pull registry.gitlab.com/offline.systems/auctioner:$(SHA)"
	-ssh $(HOST) "docker stop auctioner"
	-ssh $(HOST) "docker rm auctioner"
	ssh $(HOST) "docker run -d \
		--env-file auctioner.env \
		--env SLOWPOKER_ARGS=\"$(ARGS)\" \
		--name auctioner \
		--network datadog \
		--restart=unless-stopped \
		-p 2222:2222 \
		-p 2323:2323 \
		-p 8080:8080 \
		registry.gitlab.com/offline.systems/auctioner:$(SHA)"

	# Clean up any garbage.
	ssh $(HOST) "docker ps --filter status=exited -q | xargs -r docker rm"
	ssh $(HOST) "docker ps --filter status=created -q | xargs -r docker rm"
	ssh $(HOST) "docker volume prune -f"
	ssh $(HOST) "docker image prune -a -f"

fmt:
	@find ./config/nginx -name '*.conf' \
		| xargs -I{} -t nginxfmt {}
	@find . -name '*.go' \
		| fgrep -v '/vendor/' \
		| xargs -I{} -t goimports -w {}
	@find . -name '*.json' \
		| fgrep -v '/node_modules/' \
		| fgrep -v '/vendor/' \
		| fgrep -v package-lock.json \
		| fgrep -v package.json \
		| xargs -I{} sh -c "echo {}; jq '.' {} | sponge {}"
	@find . -name '*.yaml' \
		| fgrep -v '/node_modules/' \
		| fgrep -v '/vendor/' \
		| xargs -I{} -t yamlfmt -w {}
	@find . -name '*.yml' \
		| fgrep -v '/node_modules/' \
		| fgrep -v '/vendor/' \
		| xargs -I{} -t yamlfmt -w {}
	@find . -name '*.sh' \
		| fgrep -v '/node_modules/' \
		| fgrep -v '/vendor/' \
		| xargs -I{} -t shfmt -w -i 2 -sr {}

generate:
	go generate ./...

integration: docker-services
	# execute the tests.
	go test -tags integration -coverprofile=coverage.out -v ./...

lint:
	golangci-lint run ./...

setup:
	curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b $$(go env GOPATH)/bin v1.41.1
	go install golang.org/x/tools/cmd/goimports@latest
	go install github.com/vektra/mockery/v2@latest

auctioner:
	go build gitlab.com/offline.systems/auctioner/cmd/auctioner

test:
	go test -v -timeout 10s ./...
	go vet -composites=false $$(go list ./... | grep -v /vendor/)
	golangci-lint run ./...

upgrade:
	go get -u -t all

vendor:
	go mod tidy -go=1.17 -compat=1.17
	go mod vendor
	go mod verify

.PHONY: \
	$(BINARIES) \
	$(HOSTS:=/deploy) \
	all \
	clean \
	coverage \
	deploy \
	docker \
	docker-clean \
	generate \
	help \
	integration \
	lint \
	setup \
	test \
	upgrade \
	vendor \
