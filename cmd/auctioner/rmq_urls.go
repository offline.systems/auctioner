package main

import (
	"bytes"
	"encoding/json"
	"io"
	"log"
	"net/http"
	"os"

	"gitlab.com/jkmn/errors"
	"gitlab.com/offline.systems/auctioner/pkg/bundle"
	"gitlab.com/offline.systems/auctioner/pkg/clients/rmq"
	"gitlab.com/offline.systems/auctioner/pkg/config/keys"
	"gitlab.com/offline.systems/auctioner/pkg/control"
	"gitlab.com/offline.systems/auctioner/pkg/domcache"
	"gitlab.com/offline.systems/auctioner/pkg/model"
	"gitlab.com/offline.systems/auctioner/pkg/store"
)

type UrlProcessor struct {
	db     store.Store
	rmq    *rmq.Client
	dCache *domcache.DomCache
}

func NewUrlProcessor(ctx bundle.B, db store.Store, rmqClient *rmq.Client) *UrlProcessor {
	dCache := domcache.New(db)

	proc := &UrlProcessor{
		db:     db,
		rmq:    rmqClient,
		dCache: dCache,
	}

	return proc
}

func (proc *UrlProcessor) processQueueUrls(ctx bundle.B) {

	qName := ctx.Get(keys.QueueUrls)
	if qName == "" {
		log.Fatalf("config key %s not set", keys.QueueUrls)
	}

	deliveries, err := proc.rmq.Consume(qName)
	if err != nil {
		log.Fatal(err)
	}

	for delivery := range deliveries {
		u := &model.URL{}
		dec := json.NewDecoder(bytes.NewBuffer(delivery.Body))
		err = dec.Decode(&u)
		if err != nil {
			log.Print(errors.Stack(err))
		}

		err = proc.processUrl(ctx, u)
		if err != nil {
			log.Print(errors.Stack(err))
		}
	}
}

func (proc *UrlProcessor) processUrl(ctx bundle.B, u *model.URL) error {

	log.Printf("processing URL: %s", u.Url)

	/*
		1. Save the URL to the DB.
		2. Download the body.
		3. Scan the body for new URLs.
		4. Put the new URLs on the queue.
	*/

	u, err := proc.db.UpsertUrl(ctx, u)
	if err != nil {
		return errors.Stack(err)
	}

	if !u.RobotsPermitted || u.Blacklisted {
		// Don't download or scan.
		return nil
	}

	err = proc.download(ctx, u)
	if err != nil {
		return errors.Stack(err)
	}

	newUrls, err := proc.scanForUrls(ctx, u)
	if err != nil {
		return errors.Stack(err)
	}

	err = proc.rmq.SendUrls(ctx, newUrls)
	if err != nil {
		return errors.Stack(err)
	}

	return nil
}

func (proc *UrlProcessor) download(ctx bundle.B, u *model.URL) error {
	filepath, err := u.Path(ctx)
	if err != nil {
		return errors.Stack(err)
	}

	f, err := os.Create(filepath)
	if err != nil {
		return errors.Stack(err)
	}
	defer f.Close()

	// Let's make the request last. If we have an internal failure, we don't
	// want to waste bandwidth or lose reputation with a web server.
	resp, err := http.DefaultClient.Get(u.Url)
	if err != nil {
		return errors.Stack(err)
	}
	defer resp.Body.Close()

	_, err = io.Copy(f, resp.Body)
	if err != nil {
		return errors.Stack(err)
	}

	err = proc.db.UpdateUrlDownloaded(ctx, u.Id, resp.StatusCode)
	if err != nil {
		return errors.Stack(err)
	}

	return nil
}

func (proc *UrlProcessor) scanForUrls(ctx bundle.B, u *model.URL) ([]model.URL, error) {
	filepath, err := u.Path(ctx)
	if err != nil {
		return nil, errors.Stack(err)
	}

	f, err := os.Open(filepath)
	if err != nil {
		return nil, errors.Stack(err)
	}
	defer f.Close()

	urlStrings, err := control.ExtractURLsReader(f)
	if err != nil {
		return nil, errors.Stack(err)
	}

	urlStrings, err = control.DereferenceUrls(u.Url, urlStrings)
	if err != nil {
		return nil, errors.Stack(err)
	}

	newUrls := proc.buildUrlsFromStrings(urlStrings)

	newUrls, err = proc.applyDomainLimitsMany(ctx, newUrls)
	if err != nil {
		return nil, errors.Stack(err)
	}

	err = proc.db.UpdateUrlCrawled(ctx, u.Id)
	if err != nil {
		return nil, errors.Stack(err)
	}

	return newUrls, nil
}

func (proc *UrlProcessor) applyDomainLimitsMany(ctx bundle.B, urls []model.URL) ([]model.URL, error) {
	out := make([]model.URL, 0)

	for i := range urls {
		u := &urls[i]

		err := proc.applyRobotsTxt(ctx, u)
		if err != nil {
			return nil, errors.Stack(err)
		}

		if u.Blacklisted {
			continue
		}

		out = append(out, *u)
	}

	return out, nil
}

func (proc *UrlProcessor) applyRobotsTxt(ctx bundle.B, u *model.URL) error {
	hostname, err := u.Hostname()
	if err != nil {
		return errors.Stack(err)
	}

	dom, err := proc.dCache.Get(ctx, hostname)
	if err != nil {
		return errors.Stack(err)
	}

	if dom == nil {
		dom, err = control.AddDomain(ctx, proc.db, hostname, true)
		if err != nil {
			return errors.Stack(err)
		}
	}

	u.Blacklisted = dom.Blacklisted

	if dom.Blacklisted {
		return nil
	}

	u.RobotsPermitted, err = dom.UrlPermitted(u.Url)
	if err != nil {
		return errors.Stack(err)
	}

	return nil
}

func (proc *UrlProcessor) buildUrlsFromStrings(strings []string) []model.URL {
	out := make([]model.URL, 0)
	for _, s := range strings {
		u := model.NewUrl(s)
		out = append(out, *u)
	}
	return out
}
