package main

import (
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/offline.systems/auctioner/pkg/bundle"
	"gitlab.com/offline.systems/auctioner/pkg/clients/rmq"
	"gitlab.com/offline.systems/auctioner/pkg/config"
	"gitlab.com/offline.systems/auctioner/pkg/store"
	"gitlab.com/offline.systems/auctioner/pkg/store/mongo"
)

func main() {

	cfg := config.New()
	ctx := bundle.New(cfg)

	var db store.Store = mongo.New()

	rmqClient, err := rmq.New(ctx)
	if err != nil {
		log.Fatal(err)
	}

	urlProcessor := NewUrlProcessor(ctx, db, rmqClient)
	go urlProcessor.processQueueUrls(ctx)

	urls, err := db.GetUrlsScannedBefore(ctx, time.Now().Add(-1*24*time.Hour))
	if err != nil {
		log.Fatal(err)
	}

	time.Sleep(10 * time.Second)

	err = rmqClient.SendUrls(ctx, urls)
	if err != nil {
		log.Fatal(err)
	}

	r := mux.NewRouter()
	log.Print("listening")
	log.Fatal(http.ListenAndServe(":8080", r))
}
