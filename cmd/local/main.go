package main

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strings"

	"github.com/temoto/robotstxt"

	"gitlab.com/offline.systems/auctioner/pkg/scrape"
)

var (
	userAgent           = "quadscrape/0.1"
	hrefs               = make(map[string]bool)
	badStatuses         = make(map[string]int)
	errUnexpectedStatus = errors.New("unexpected status code")
)

func main() {
	client := &http.Client{}

	domain := "www.racedayquads.com"

	robots, err := getRobotData(client, domain)
	if err != nil {
		log.Fatal(err)
	}

	hrefs["/"] = false

	for next := getNextHref(); next != ""; next = getNextHref() {
		anchors, status, err := Visit(client, robots, domain, next)
		if errors.Is(err, errUnexpectedStatus) {
			badStatuses[next] = status
			hrefs[next] = true
			continue
		}
		if err != nil {
			log.Fatal(err)
		}

		hrefs[next] = true
		updateHrefs(anchors)

		qty := 0
		qtyVisited := 0

		for _, visited := range hrefs {
			qty++
			if visited {
				qtyVisited++
			}
		}
		log.Printf("TOTAL: %d\tVISITED: %d", qty, qtyVisited)
	}
}

func getNextHref() string {
	for a, visited := range hrefs {
		if !visited {
			return a
		}
	}

	return ""
}

func updateHrefs(anchors []string) {
	for _, a := range anchors {
		_, ok := hrefs[a]
		if ok {
			continue
		}
		hrefs[a] = false
	}
}

type Motor struct {
	Kv     int
	M      float64 // grams
	Price  float64 // USD
	AMax   float64 // amps
	AIdle  float64 // amps
	VMin   float64 // volts
	VMax   float64 // volts
	DShaft float64 // mm
	Size   string  //
}

func getLocalPath(domain, path string) string {
	localPath := path
	if localPath == "/" {
		localPath = "root"
	}
	localPath = filepath.Join(".", domain, localPath)
	if !(strings.HasSuffix(localPath, ".htm") || (strings.HasSuffix(localPath, ".html"))) {
		localPath = fmt.Sprintf("%s.html", localPath)
	}

	return localPath
}

func Visit(client *http.Client, robots *robotstxt.RobotsData, domain, path string) ([]string, int, error) {

	r, status, err := Get(client, domain, path)
	if err != nil {
		return nil, status, err
	}

	anchors, err := scrape.GetAnchors(r)
	if err != nil {
		return nil, status, err
	}

	anchors, err = scrape.FilterAnchorsByDomain(anchors, domain)
	if err != nil {
		return nil, status, err
	}

	anchors = scrape.FilterAllowedAnchors(robots, userAgent, anchors)

	return anchors, status, nil
}

func assureParentDirectories(path string) error {
	parent := filepath.Dir(path)
	err := os.MkdirAll(parent, os.ModeDir|0o777)
	if err != nil {
		return err
	}

	return nil
}

func existsFile(path string) (bool, error) {
	fi, err := os.Stat(path)
	if os.IsNotExist(err) {
		return false, nil
	}
	if err != nil {
		return false, err
	}
	if fi.IsDir() {
		return true, errors.New("expected file is a directory")
	}
	if err != nil {
		return false, err
	}

	return true, nil
}

func Get(client *http.Client, domain, path string) (io.Reader, int, error) {

	localPath := getLocalPath(domain, path)

	var respBytes []byte

	err := assureParentDirectories(localPath)
	if err != nil {
		return nil, 0, err
	}

	exists, err := existsFile(localPath)
	if err != nil {
		return nil, 0, err
	}

	var statusCode int

	if exists {
		log.Printf("USING LOCAL FILE: %s", localPath)
		respBytes, err = os.ReadFile(localPath)
		if err != nil {
			return nil, statusCode, err
		}
	} else {
		log.Printf("FETCHING NEW COPY: %s", localPath)
		u := url.URL{
			Scheme: "https",
			Host:   domain,
			Path:   path,
		}

		req, err := http.NewRequest("GET", u.String(), nil)
		if err != nil {
			return nil, 0, err
		}

		req.Header.Set("User-Agent", userAgent)

		resp, err := client.Do(req)
		if err != nil {
			return nil, resp.StatusCode, err
		}
		defer resp.Body.Close()
		statusCode = resp.StatusCode

		if resp.StatusCode < 200 || resp.StatusCode >= 300 {
			return nil, statusCode, errUnexpectedStatus
		}

		respBytes, err = io.ReadAll(resp.Body)
		if err != nil {
			return nil, statusCode, err
		}
	}

	if !exists {
		f, err := os.Create(localPath)
		if err != nil {
			return nil, statusCode, err
		}

		_, err = io.Copy(f, bytes.NewReader(respBytes))
		if err != nil {
			return nil, statusCode, err
		}
	}

	return bytes.NewReader(respBytes), statusCode, nil
}

func getRobotData(client *http.Client, domain string) (*robotstxt.RobotsData, error) {
	// https://www.racedayquads.com/robots.txt

	r, statusCode, err := Get(client, domain, "/robots.txt")
	if errors.Is(err, errUnexpectedStatus) {
		return nil, fmt.Errorf("unexpected status code fetching robots.txt: %d", statusCode)
	}
	if err != nil {
		return nil, err
	}

	b, err := io.ReadAll(r)
	if err != nil {
		return nil, err
	}

	return robotstxt.FromBytes(b)
}
