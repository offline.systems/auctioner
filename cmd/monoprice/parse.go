package main

import (
	"io"
	"log"
	"strings"

	"golang.org/x/net/html"
	"golang.org/x/net/html/atom"
)

type ParseResult struct {
	optionMap    map[string][]string
	selection    Selection
	outOfStock   map[string]string
	notAvailable map[string]string
	price        string
	upc          string
}

func (r *ParseResult) AddCategory(category string) {
	r.optionMap[category] = make([]string, 0)
}

func (r *ParseResult) AddOption(category, option string) {
	options := r.optionMap[category]
	options = append(options, option)
	r.optionMap[category] = options
}

func (r *ParseResult) SetPrice(s string) {
	s = strings.TrimSpace(s)
	s = strings.TrimPrefix(s, "$")
	r.price = s
}

func (r *ParseResult) SetUPC(s string) {
	s = strings.TrimSpace(s)
	s = strings.TrimPrefix(s, "UPC")
	s = strings.TrimSpace(s)
	s = strings.TrimPrefix(s, "#")
	s = strings.TrimSpace(s)
	r.upc = s
}

func (r *ParseResult) RecordOutOfStock(category string, option string) {
	r.outOfStock[category] = option
}

func (r *ParseResult) RecordNotAvailable(category string, option string) {
	r.notAvailable[category] = option
}

func parse(r io.Reader) ParseResult {

	result := ParseResult{
		optionMap:    make(map[string][]string),
		selection:    make(Selection),
		outOfStock:   make(map[string]string),
		notAvailable: make(map[string]string),
	}

	var (
		category    string
		nextIsPrice = false
		nextIsUPC   = false
	)

	z := html.NewTokenizer(r)
	for {
		tt := z.Next()
		if tt == html.ErrorToken {
			tok := z.Token()
			log.Print(tt.String())
			log.Print(tok.String())
			break
		}

		tok := z.Token()

		if nextIsPrice {
			nextIsPrice = false
			result.SetPrice(tok.Data)
		}

		if nextIsUPC {
			nextIsUPC = false
			result.SetUPC(tok.Data)
		}

		if tok.DataAtom == atom.Form {
			val := getAttr(tok, "data-mp-attrname")
			if val != "" {
				category = val

				option := getAttr(tok, "data-mp-attrselval")
				result.selection[category] = option
			}
		}

		if tok.DataAtom == atom.Div {
			if hasClass(tok, "product-barcode") {
				nextIsUPC = true
			}
		}

		if tok.DataAtom == atom.Span {
			option := getAttr(tok, "data-mp-attrval")
			if option != "" {
				result.AddOption(category, option)

				// note the option as out of stock
				if hasClass(tok, "outofstock") {
					result.RecordOutOfStock(category, option)
				}
				if hasClass(tok, "na") {
					result.RecordNotAvailable(category, option)
				}
			}

			if hasClass(tok, "sale-price") {
				nextIsPrice = true
			}
		}

		// log.Print(tok.String())
	}

	return result
}

func getAttr(tok html.Token, key string) string {
	attrs := tok.Attr
	for _, attr := range attrs {
		if attr.Key == key {
			return attr.Val
		}
	}
	return ""
}

func hasClass(tok html.Token, class string) bool {
	classes := getAttr(tok, "class")
	chunks := strings.Split(classes, " ")
	for _, chunk := range chunks {
		if chunk == class {
			return true
		}
	}
	return false
}
