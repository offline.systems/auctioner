package main

type CombinationSet struct {
	combos []Combination
}

func (cs CombinationSet) SetOutOfStock(selection Selection) {
	c := cs.GetCombination(selection)
	if c == nil {
		return
	}

	c.OutOfStock = true
}

func (cs CombinationSet) SetNotAvailable(selection Selection) {
	c := cs.GetCombination(selection)
	if c == nil {
		return
	}

	c.NotAvailable = true
}

func (cs CombinationSet) SetPrice(selection Selection, price string) {
	c := cs.GetCombination(selection)
	if c == nil {
		return
	}

	c.Price = price
}

func (cs CombinationSet) GetCombination(selection Selection) *Combination {
	for i, c := range cs.combos {
		if c.HasSelection(selection) {
			return &cs.combos[i]
		}
	}
	return nil
}

func (cs CombinationSet) NextSelection() *Selection {
	for i := range cs.combos {
		c := cs.combos[i]
		if c.OutOfStock || c.NotAvailable || c.Price != "" {
			continue
		}
		return &c.Selection
	}

	return nil
}

type Combination struct {
	Selection    Selection // map[category]option
	OutOfStock   bool
	NotAvailable bool
	Price        string
}

func (c Combination) HasSelection(selection Selection) bool {
	if len(c.Selection) != len(selection) {
		return false
	}

	for k, v := range c.Selection {
		if selection[k] != v {
			return false
		}
	}

	return true
}

func generateAllCombos(options map[string][]string) []Combination {
	tree := generateTree(options)
	combos := tree.Coallesce()
	return combos
}
