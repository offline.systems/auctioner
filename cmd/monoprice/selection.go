package main

type Selection map[string]string

func (s Selection) Replace(category, option string) Selection {
	out := make(Selection)
	for k, v := range s {
		out[k] = v
	}

	out[category] = option
	return out
}
