package main

func generateTree(options map[string][]string) *Node {

	// Let's create a tree.
	root := &Node{}

	for category, options := range options {
		root.PushOptions(category, options)
	}

	return root
}

type Node struct {
	Category string
	Option   string
	Children []*Node
}

func (node *Node) PushOptions(category string, options []string) {
	if node.Children != nil {
		for _, c := range node.Children {
			c.PushOptions(category, options)
		}
		return
	}

	node.Children = make([]*Node, 0)
	for _, opt := range options {
		c := &Node{
			Category: category,
			Option:   opt,
		}
		node.Children = append(node.Children, c)
	}
}

func (node *Node) Coallesce() []Combination {
	if node.Children == nil {
		selections := map[string]string{
			node.Category: node.Option,
		}
		combo := Combination{
			Selection: selections,
		}
		return []Combination{combo}
	}

	combos := make([]Combination, 0)
	for _, c := range node.Children {
		childCombos := c.Coallesce()
		combos = append(combos, childCombos...)
	}

	if node.Category == "" {
		// This is the root node.
		return combos
	}

	// Add this nodes selection to the children.
	for _, combo := range combos {
		combo.Selection[node.Category] = node.Option
	}

	return combos
}
