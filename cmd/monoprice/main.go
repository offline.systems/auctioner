package main

import (
	"bytes"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"

	"gitlab.com/jkmn/errors"
)

func main() {

	// f, err := os.Open("data.json")
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// defer f.Close()

	// pv := &ProductView{}
	// dec := json.NewDecoder(f)
	// err = dec.Decode(pv)
	// if err != nil {
	// 	log.Fatal(err)
	// }

	// r := parse(bytes.NewBufferString(pv.InfoPartialView))

	f, err := os.Open("monoprice_product.html")
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	r := parse(f)

	// jlog(optionMap)
	// jlog(current)

	allCombos := generateAllCombos(r.optionMap)
	// jlog(allCombos)

	cs := CombinationSet{combos: allCombos}

	// update out-of-stock
	for category, option := range r.outOfStock {
		selection := r.selection.Replace(category, option)
		cs.SetOutOfStock(selection)
	}

	// update unavailable
	for category, option := range r.notAvailable {
		selection := r.selection.Replace(category, option)
		cs.SetNotAvailable(selection)
	}

	// jlog(notAvailable)
	jlog(r.price)
	jlog(r.upc)

	jlog(r.selection)
	next := cs.NextSelection()
	jlog(cs.NextSelection())
	if next != nil {
		combo := cs.GetCombination(*next)
		jlog(combo)
	}

	_, _ = fetchSelectPid(9842, r.selection)
}

func fetchSelectPid(id int, selection Selection) (io.Reader, error) {
	// https://www.monoprice.com/product/selectpid?vals=50ft&vals=Yellow&vals=1%20Pack&PID=9842&changedVal=50ft

	u, err := url.Parse("https://www.monoprice.com/product/selectpid")
	if err != nil {
		return nil, errors.Stack(err)
	}

	vals := url.Values{}
	vals.Add("PID", fmt.Sprintf("%d", id))
	for _, opt := range selection {
		vals.Add("vals", opt)
	}

	u.RawQuery = vals.Encode()

	log.Print(u.String())
	return nil, nil
}

func fetchProduct(id int) (io.Reader, error) {
	// https://www.monoprice.com/product?p_id=9872
	u := fmt.Sprintf("https://www.monoprice.com/product?p_id=%d", id)
	resp, err := http.Get(u)
	if err != nil {
		return nil, errors.Stack(err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		err := errors.New("did not get 200")
		err = err.OfStatus(resp.StatusCode)
		return nil, err
	}

	buf := bytes.NewBuffer(nil)
	_, err = io.Copy(buf, resp.Body)
	if err != nil {
		return nil, errors.Stack(err)
	}

	return buf, nil
}

type ProductView struct {
	ImagePartialView string `json:"imagePartialView"`
	InfoPartialView  string `json:"infoPartialView"`
	DescPartialView  string `json:"descPartialView"`
	SpecPartialView  string `json:"specPartialView"`
}
