package main

import (
	"bytes"
	"encoding/json"
	"log"
)

func jlog(a interface{}) {
	buf := bytes.NewBuffer(nil)
	enc := json.NewEncoder(buf)
	enc.SetIndent("", "  ")
	err := enc.Encode(a)
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("\n%s", buf.String())
}
