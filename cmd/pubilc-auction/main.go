package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"regexp"
	"strconv"
	"strings"

	"gitlab.com/jkmn/errors"
	"golang.org/x/net/html"
)

func main() {
	filename := "public-auction-1.html"
	uIndex := "https://www.publicsurplus.com/sms/boxeldercounty,ut/list/current?orgid=14771"
	// filename := "public-auction-2.html"
	// uIndex := "https://www.publicsurplus.com/sms/boxeldercounty,ut/list/current?slth=&sma=&orgid=14771&sorg=&ctxnId=338621455&page=1&sortBy=timeLeft"

	err := downloadPage(filename, uIndex)
	if err != nil {
		log.Fatal(errors.Stack(err))
	}

	f, err := os.Open(filename)
	if err != nil {
		log.Fatal(errors.Stack(err))
	}
	defer f.Close()

	hrefs, err := grabAuctionRefs(f)
	if err != nil {
		log.Fatal(errors.Stack(err))
	}

	hrefs = filterHrefs(hrefs)

	urls, err := normalizeHrefs(uIndex, hrefs)
	if err != nil {
		log.Fatal(errors.Stack(err))
	}

	auctionDatas := make([]AuctionData, 0)

	for _, u := range urls {
		log.Print(u)
		auctionFileName, err := downloadAuctionPage(u)
		if err != nil {
			log.Fatal(errors.Stack(err))
		}

		ad, err := extractAuctionData(auctionFileName)
		if err != nil {
			log.Fatal(errors.Stack(err))
		}

		auctionDatas = append(auctionDatas, *ad)
	}

	for _, ad := range auctionDatas {
		fmt.Println(ad.String())
	}
}

func downloadPage(filename, url string) error {
	if existsFile(filename) {
		return nil
	}

	resp, err := http.Get(url)
	if err != nil {
		return errors.Stack(err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return errors.New("non-200 error code")
	}

	f, err := os.Create(filename)
	if err != nil {
		return errors.Stack(err)
	}
	defer f.Close()

	_, err = io.Copy(f, resp.Body)
	if err != nil {
		return errors.Stack(err)
	}

	log.Print("downloaded page")

	return nil
}

func grabAuctionRefs(r io.Reader) ([]string, error) {
	root, err := html.Parse(r)
	if err != nil {
		return nil, errors.Stack(err)
	}

	out, err := extractNodeHrefs(root)
	if err != nil {
		return nil, errors.Stack(err)
	}

	return out, nil
}

func extractNodeHrefs(n *html.Node) ([]string, error) {

	out := make([]string, 0)

	if n.Type == html.ElementNode && n.Data == "a" {
		href := extractHref(n)
		if href != "" {
			out = append(out, href)
		}
	}

	for c := n.FirstChild; c != nil; c = c.NextSibling {
		out_, err := extractNodeHrefs(c)
		if err != nil {
			return nil, errors.Stack(err)
		}

		out = append(out, out_...)
	}

	return out, nil
}

func extractHref(n *html.Node) string {
	for _, attr := range n.Attr {
		if attr.Key == "href" {
			return attr.Val
		}
	}

	return ""
}

func filterHrefs(hrefs []string) []string {
	out := make([]string, 0)

	for _, href := range hrefs {
		if strings.HasPrefix(href, "javascript") {
			continue
		}
		if strings.HasPrefix(href, "mailto:") {
			continue
		}
		if strings.HasPrefix(href, "#") {
			continue
		}
		if !strings.Contains(href, "view?auc=") {
			continue
		}
		out = append(out, href)
	}

	return out
}

func normalizeHrefs(src string, hrefs []string) ([]string, error) {

	uSrc, err := url.Parse(src)
	if err != nil {
		return nil, errors.Stack(err)
	}

	out := make([]string, 0)
	for _, href := range hrefs {
		u, err := url.Parse(href)
		if err != nil {
			return nil, errors.Stack(err)
		}

		u = uSrc.ResolveReference(u)
		out = append(out, u.String())
	}

	return out, nil
}

func downloadAuctionPage(s string) (string, error) {
	u, err := url.Parse(s)
	if err != nil {
		return "", errors.Stack(err)
	}

	auctionNumber := u.Query().Get("auc")
	if auctionNumber == "" {
		return "", errors.Newf("no auction number detected: %s", s)
	}

	filename := fmt.Sprintf("%s.html", auctionNumber)

	err = downloadPage(filename, s)
	if err != nil {
		return "", errors.Stack(err)
	}

	return filename, nil
}

func existsFile(path string) bool {
	f, err := os.Open(path)
	if os.IsNotExist(err) {
		return false
	}
	if err != nil {
		log.Fatal(errors.Stack(err))
	}
	defer f.Close()
	return true
}

type AuctionData struct {
	Price     float64
	Area      string
	AccountNo string
}

func (d AuctionData) String() string {
	return fmt.Sprintf("%s,%.2f,%s", d.AccountNo, d.Price, d.Area)
}

func extractAuctionData(filename string) (*AuctionData, error) {
	f, err := os.Open(filename)
	if err != nil {
		return nil, errors.Stack(err)
	}
	defer f.Close()

	root, err := html.Parse(f)
	if err != nil {
		return nil, errors.Stack(err)
	}

	price := extractPrice(root)

	acres := extractArea(root)

	accountNo := extractAccountNo(root)

	out := &AuctionData{
		Price:     price,
		Area:      acres,
		AccountNo: accountNo,
	}

	return out, nil

}

// 40 AC
var rxAcres = regexp.MustCompile(`[\d\.,]+ AC`)
var rxSqFt = regexp.MustCompile(`[\d\.,]+ SQ FT`)

func extractArea(n *html.Node) string {

	if n.Type == html.TextNode {
		s := n.Data

		match := rxAcres.FindString(s)
		if match != "" {
			return match
		}

		match = rxSqFt.FindString(s)
		if match != "" {
			return match
		}
	}

	for c := n.FirstChild; c != nil; c = c.NextSibling {
		acres := extractArea(c)
		if acres != "" {
			return acres
		}
	}

	return ""
}

func extractAccountNo(n *html.Node) string {
	var acctNo string

	if n.Type == html.ElementNode && n.Data == "h4" {
		acctNo = extractAccountString(n.FirstChild)
		if acctNo != "" {
			return acctNo
		}
	}

	for c := n.FirstChild; c != nil; c = c.NextSibling {
		acctNo = extractAccountNo(c)
		if acctNo != "" {
			return acctNo
		}
	}

	return ""
}

// R0036133
var rxAcctNo = regexp.MustCompile(`R\d{7}`)

func extractAccountString(n *html.Node) string {
	if n.Type != html.TextNode {
		return ""
	}

	s := n.Data

	match := rxAcctNo.FindString(s)
	return match
}

func extractPrice(n *html.Node) float64 {

	var price float64

	if n.Type == html.ElementNode && n.Data == "td" {
		price = extractPriceString(n.FirstChild)
		if price != 0 {
			return price
		}
	}

	for c := n.FirstChild; c != nil; c = c.NextSibling {
		price = extractPrice(c)
		if price != 0 {
			return price
		}
	}

	return 0
}

func extractPriceString(n *html.Node) float64 {
	if n.Type != html.TextNode {
		return 0
	}

	s := n.Data

	s = strings.TrimSpace(s)
	s = strings.ReplaceAll(s, ",", "")
	s = strings.TrimPrefix(s, "$")

	f, _ := strconv.ParseFloat(s, 64)

	return f
}
