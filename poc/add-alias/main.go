package main

import (
	"flag"
	"log"

	"gitlab.com/offline.systems/auctioner/pkg/bundle"
	"gitlab.com/offline.systems/auctioner/pkg/config"
	"gitlab.com/offline.systems/auctioner/pkg/control"
	"gitlab.com/offline.systems/auctioner/pkg/store"
	"gitlab.com/offline.systems/auctioner/pkg/store/mongo"
)

func main() {

	flag.Parse()

	hostname := flag.Arg(0)
	if hostname == "" {
		log.Fatal("expecting two argument arguments, the domain and the alias")
	}

	alias := flag.Arg(1)
	if alias == "" {
		log.Fatal("expecting two argument arguments, the domain and the alias")
	}

	cfg := config.New()
	ctx := bundle.New(cfg)
	var s store.Store = mongo.New()

	err := control.AddDomainAlias(ctx, s, hostname, alias)
	if err != nil {
		log.Fatal(err)
	}
}
