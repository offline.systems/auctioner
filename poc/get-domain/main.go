package main

import (
	"flag"
	"log"

	"gitlab.com/offline.systems/auctioner/pkg/bundle"
	"gitlab.com/offline.systems/auctioner/pkg/config"
	"gitlab.com/offline.systems/auctioner/pkg/store"
	"gitlab.com/offline.systems/auctioner/pkg/store/mongo"
)

func main() {

	flag.Parse()

	hostname := flag.Arg(0)
	if hostname == "" {
		log.Fatal("expecting one argument, the domain name")
	}

	cfg := config.New()
	ctx := bundle.New(cfg)
	var s store.Store = mongo.New()

	dom, err := s.GetDomain(ctx, hostname)
	if err != nil {
		log.Fatal(err)
	}

	ctx.JLog(dom)
}
