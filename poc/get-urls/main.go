package main

import (
	"log"
	"time"

	"gitlab.com/offline.systems/auctioner/pkg/bundle"
	"gitlab.com/offline.systems/auctioner/pkg/config"
	"gitlab.com/offline.systems/auctioner/pkg/store"
	"gitlab.com/offline.systems/auctioner/pkg/store/mongo"
)

func main() {

	cfg := config.New()
	ctx := bundle.New(cfg)
	var s store.Store = mongo.New()

	dom, err := s.GetUrlsScannedBefore(ctx, time.Now().Add(-1*24*time.Hour))
	if err != nil {
		log.Fatal(err)
	}

	ctx.JLog(dom)
}
