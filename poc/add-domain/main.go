package main

import (
	"flag"
	"log"

	"gitlab.com/offline.systems/auctioner/pkg/bundle"
	"gitlab.com/offline.systems/auctioner/pkg/config"
	"gitlab.com/offline.systems/auctioner/pkg/control"
	"gitlab.com/offline.systems/auctioner/pkg/store"
	"gitlab.com/offline.systems/auctioner/pkg/store/mongo"
)

var (
	blacklisted = flag.Bool("blacklist", false, "save the domain as blacklisted")
)

func main() {

	flag.Parse()

	domainString := flag.Arg(0)
	if domainString == "" {
		log.Fatal("expecting one argument, a domain")
	}

	cfg := config.New()
	ctx := bundle.New(cfg)
	var s store.Store = mongo.New()

	_, err := control.AddDomain(ctx, s, domainString, *blacklisted)
	if err != nil {
		log.Fatal(err)
	}
}
