package errors

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
)

type Error struct {
	root    error
	Frames  []frame
	status  int
	addenda []string
}

func New(msg string) *Error {

	newError := &Error{
		root:    errors.New(msg),
		Frames:  getFrames(),
		addenda: make([]string, 0),
	}

	return newError
}

func Newf(format string, args ...interface{}) *Error {
	return New(fmt.Sprintf(format, args...))
}

func Stack(err error, addenda ...string) *Error {

	if err == nil {
		return nil
	}

	err_, ok := err.(*Error)
	if ok {
		err_.addenda = append(err_.addenda, addenda...)
		return err_
	}

	newError := &Error{
		root:    err,
		Frames:  getFrames(),
		addenda: addenda,
	}

	return newError
}

func (err *Error) Root() error {
	return err.root
}

func (err *Error) Error() string {
	b := bytes.NewBuffer(nil)
	fmt.Fprintln(b, err.root)
	for _, f := range err.Frames {
		fmt.Fprintf(b, "%s:%d\t(%s)\n", f.File, f.Line, f.Func)
	}
	return b.String()
}

func (err *Error) Stack() string {
	b := bytes.NewBuffer(nil)
	for _, f := range err.Frames {
		fmt.Fprint(b, f.String())
	}
	return b.String()
}

func (err *Error) RootError() error {
	return err.root
}

func (err *Error) Status() int {
	return err.status
}

func (err *Error) OfStatus(status int) *Error {
	err.status = status
	return err
}

type jsonError struct {
	Root    string   `json:"root"`
	Frames  []frame  `json:"frames,omitempty"`
	Status  int      `json:"status,omitempty"`
	Addenda []string `json:"addenda,omitempty"`
}

func (err *Error) MarshalJSON() ([]byte, error) {
	e := jsonError{
		Root:    err.root.Error(),
		Frames:  err.Frames,
		Status:  err.status,
		Addenda: err.addenda,
	}

	return json.Marshal(e)
}
