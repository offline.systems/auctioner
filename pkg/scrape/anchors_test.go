package scrape_test

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/temoto/robotstxt"

	"gitlab.com/offline.systems/auctioner/pkg/scrape"
)

func TestGetAnchors(t *testing.T) {
	a := assert.New(t)

	f, err := os.Open("rdq.html")
	a.NoError(err)

	anchors, err := scrape.GetAnchors(f)
	a.NoError(err)

	for _, anchor := range anchors {
		t.Log(anchor)
	}
}

func TestFilterAnchorsByDomain(t *testing.T) {
	a := assert.New(t)

	f, err := os.Open("rdq.html")
	a.NoError(err)

	anchors, err := scrape.GetAnchors(f)
	a.NoError(err)

	anchors, err = scrape.FilterAnchorsByDomain(anchors, "www.racedayquads.com")
	a.NoError(err)

	for _, anchor := range anchors {
		t.Log(anchor)

		localPath := filepath.Join(".", anchor)
		if !(strings.HasSuffix(localPath, ".htm") || (strings.HasSuffix(localPath, ".html"))) {
			localPath = fmt.Sprintf("%s.html", localPath)
		}
		t.Log(localPath)
	}
}

func TestAnchors(t *testing.T) {
	a := assert.New(t)

	testCases := []string{
		"../www.racedayquads.com/collections/2s-batteries/manufacturer_auline.html",
		"rdq.html",
	}

	for _, tc := range testCases {
		t.Run(tc, func(t *testing.T) {

			f, err := os.Open(tc)
			a.NoError(err)

			bRobots, err := os.ReadFile("rdq_robots.txt")
			a.NoError(err)

			robots, err := robotstxt.FromBytes(bRobots)
			a.NoError(err)

			anchors, err := scrape.GetAnchors(f)
			a.NoError(err)

			anchors = scrape.FilterAllowedAnchors(robots, "test", anchors)

			anchors, err = scrape.FilterAnchorsByDomain(anchors, "www.racedayquads.com")
			a.NoError(err)

			for _, anchor := range anchors {
				t.Log(anchor)
			}
		})
	}

}
