package scrape

import (
	"io"
	"log"
	"net/url"
	"sort"
	"strings"

	"github.com/temoto/robotstxt"
	"golang.org/x/net/html"
)

func FilterAllowedAnchors(robots *robotstxt.RobotsData, agent string, anchors []string) []string {
	out := make([]string, 0)
	for _, a := range anchors {
		allowed := robots.TestAgent(a, agent)
		if allowed {
			out = append(out, a)
		}
	}
	return out
}

func FilterAnchorsByDomain(anchors []string, domain string) ([]string, error) {

	out := make([]string, 0)
	for _, a := range anchors {
		u, err := url.Parse(a)
		if err != nil {
			return nil, err
		}

		if u.Scheme == "tel" {
			continue
		}

		if u.Host != domain && u.Host != "" {
			continue
		}

		out = append(out, u.Path)
	}

	return out, nil
}

func cleanAnchors(in []string) []string {

	// this is a sanity check.
	// there are malformed queries that get parsed as paths.

	in_ := make([]string, 0)

	for _, i := range in {
		u, err := url.Parse(i)
		if err != nil {
			log.Printf("failed to parse reference: %s", i)
			log.Print(err)
			continue
		}

		if strings.Contains(u.Path, "&") {
			log.Printf("reference ingnored: %s", i)
			continue
		}

		in_ = append(in_, i)
	}

	m := make(map[string]struct{})
	for _, k := range in_ {
		m[k] = struct{}{}
	}

	out := make([]string, 0, len(m))
	for k := range m {
		out = append(out, k)
	}

	sort.Strings(out)

	return out
}

func GetAnchors(r io.Reader) ([]string, error) {
	z := html.NewTokenizer(r)

	anchors := make([]string, 0)

	for {
		tt := z.Next()
		if tt == html.ErrorToken {
			if z.Err() == io.EOF {
				anchors = cleanAnchors(anchors)
				return anchors, nil
			}
			return nil, z.Err()
		}
		if tt == html.StartTagToken {
			name, hasAttr := z.TagName()
			if len(name) > 1 {
				continue
			}
			if name[0] != 'a' {
				continue
			}
			if !hasAttr {
				continue
			}

			attrs := getAttrs(z)

			href, ok := attrs["href"]
			if !ok {
				continue
			}

			if strings.HasPrefix(href, "#") {
				continue
			}

			anchors = append(anchors, href)
		}
	}
}

func getAttrs(z *html.Tokenizer) map[string]string {
	attrs := make(map[string]string)
	more := true
	for more {
		var (
			key_, val_ []byte
		)
		key_, val_, more = z.TagAttr()
		key := string(key_)
		val := string(val_)
		attrs[key] = val
	}

	return attrs
}
