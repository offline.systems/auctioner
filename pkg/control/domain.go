package control

import (
	"log"

	"gitlab.com/jkmn/errors"
	"gitlab.com/offline.systems/auctioner/pkg/bundle"
	"gitlab.com/offline.systems/auctioner/pkg/model"
	"gitlab.com/offline.systems/auctioner/pkg/store"
)

func AddDomain(ctx bundle.B, s store.Store, domainString string, blacklisted bool) (*model.Domain, error) {

	robotsTxt, robotsUrl, err := GetRobotsTxt(domainString)
	if err != nil {
		if blacklisted {
			log.Print(errors.Stack(err))
		} else {
			return nil, errors.Stack(err)
		}
	}

	dom := &model.Domain{
		Domain:       domainString,
		RobotsTxtRaw: robotsTxt,
		RobotsTxtUrl: robotsUrl,
		Blacklisted:  blacklisted,
	}

	dom, err = s.InsertDomain(ctx, dom)
	if err != nil {
		return dom, errors.Stack(err)
	}

	if blacklisted {
		return dom, nil
	}

	// bootstrap a URL to be scanned.
	permitted, err := dom.UrlPermitted(dom.Url())
	if err != nil {
		return dom, errors.Stack(err)
	}

	u := &model.URL{
		Url:             dom.Url(),
		RobotsPermitted: permitted,
		Created:         dom.Created,
	}
	_, err = s.UpsertUrl(ctx, u)
	if err != nil {
		return dom, errors.Stack(err)
	}

	return dom, nil
}

func AddDomainAlias(ctx bundle.B, s store.Store, hostname, alias string) error {
	dom, err := s.GetDomain(ctx, hostname)
	if err != nil {
		return errors.Stack(err)
	}

	dom.AddAlias(alias)

	_, err = s.ReplaceDomain(ctx, dom)
	if err != nil {
		return errors.Stack(err)
	}

	return nil
}
