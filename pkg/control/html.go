package control

import (
	"bytes"
	"io"
	"net/url"
	"strings"

	"gitlab.com/jkmn/errors"
	"golang.org/x/net/html"
	"golang.org/x/net/html/atom"
)

func ExtractURLsBytes(raw []byte) ([]string, error) {
	r := bytes.NewBuffer(raw)
	return ExtractURLsReader(r)
}

func ExtractURLsReader(r io.Reader) ([]string, error) {
	z := html.NewTokenizer(r)

	var (
		urls []string = make([]string, 0)
	)

	for {
		tt := z.Next()

		if tt == html.ErrorToken {
			break
		}

		tok := z.Token()

		if tok.DataAtom == atom.A {
			u := getHref(tok)
			u = sanitizeUrl(u)
			if u != "" {
				urls = append(urls, u)
			}
		}
	}

	return urls, nil
}

func sanitizeUrl(u string) string {
	if u == "" {
		return ""
	}

	out := u

	badPrefixes := []string{
		"#",
		"javascript",
		"tel:",
	}

	for _, badPrefix := range badPrefixes {
		if strings.HasPrefix(out, badPrefix) {
			return ""
		}
	}

	return out
}

func getHref(tok html.Token) string {
	for _, attr := range tok.Attr {
		if attr.Key == "href" {
			return attr.Val
		}
	}

	return ""
}

func DereferenceUrls(ref string, urls []string) ([]string, error) {
	rootUrl, err := url.Parse(ref)
	if err != nil {
		return nil, errors.Stack(err)
	}

	sanitized := make([]string, 0)

	for _, u := range urls {
		u_, err := url.Parse(u)
		if err != nil {
			return nil, errors.Stack(err)
		}

		u_ = rootUrl.ResolveReference(u_)
		sanitized = append(sanitized, u_.String())
	}

	return sanitized, nil
}
