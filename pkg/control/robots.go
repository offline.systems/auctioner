package control

import (
	"bytes"
	"fmt"
	"io"
	"net/http"

	"gitlab.com/jkmn/errors"
)

func GetRobotsTxt(domain string) ([]byte, string, error) {
	u := fmt.Sprintf("https://%s/robots.txt", domain)
	buf := bytes.NewBuffer(nil)

	resp, err := http.Get(u)
	if err != nil {
		return nil, "", errors.Stack(err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return nil, "", errors.Newf("status code: %d", resp.StatusCode)
	}

	_, _ = io.Copy(buf, resp.Body)

	return buf.Bytes(), u, nil
}
