package control

import (
	"bytes"
	"io"
	"net/http"

	"gitlab.com/jkmn/errors"
)

func Get(u string) ([]byte, error) {
	req, err := http.NewRequest("GET", u, nil)
	if err != nil {
		return nil, errors.Stack(err)
	}

	req.Header.Set("User-Agent", "OfflineSystemsBot/0.1 (+https://offline.systems/bot.html)")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, errors.Stack(err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return nil, errors.Newf("status code: %d", resp.StatusCode)
	}

	b := bytes.NewBuffer(nil)

	_, err = io.Copy(b, resp.Body)
	if err != nil {
		return nil, errors.Stack(err)
	}

	return b.Bytes(), nil
}
