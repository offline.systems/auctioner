package control_test

import (
	"io/ioutil"
	"log"
	"net/url"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/offline.systems/auctioner/pkg/control"
)

func TestExtractURLs_Monoprice(t *testing.T) {
	a := assert.New(t)

	b, err := ioutil.ReadFile("monoprice-root.html")
	a.NoError(err)

	urls, err := control.ExtractURLsBytes(b)
	a.NoError(err)

	a.Len(urls, 1453, "expected %d", len(urls))

	rootUrl, err := url.Parse("https://www.monoprice.com")
	a.NoError(err)

	sanitized := make([]string, 0)

	for _, u := range urls {
		u_, err := url.Parse(u)
		a.NoError(err)

		u_ = rootUrl.ResolveReference(u_)
		sanitized = append(sanitized, u_.String())
	}

	for _, u := range sanitized {
		log.Print(u)
	}
}
