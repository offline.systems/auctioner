package model

import (
	"time"

	"github.com/shopspring/decimal"
)

type Product struct {
	Id        string `bson:"_id"`
	Created   time.Time
	Updated   time.Time
	UPC       string
	HumanURL  string
	Variation map[string]string
	Price     decimal.Decimal
}

type Variation struct {
	UPC string
}
