package model

import (
	"fmt"
	"net/url"
	"path"
	"time"

	"gitlab.com/jkmn/errors"
	"gitlab.com/offline.systems/auctioner/pkg/bundle"
	"gitlab.com/offline.systems/auctioner/pkg/config/keys"
)

type URL struct {
	Id              string `bson:"_id"`
	Url             string
	RobotsPermitted bool
	Blacklisted     bool
	Created         time.Time
	Downloaded      time.Time
	Crawled         time.Time
	LastStatus      uint
}

func NewUrl(s string) *URL {
	u := &URL{
		Url:     s,
		Created: time.Now(),
	}
	return u
}

func (u *URL) Path(ctx bundle.B) (string, error) {
	root := ctx.Get(keys.PathPages)
	if root == "" {
		return "", errors.Newf("config key %s not set", keys.PathPages)
	}

	p := path.Join(root, u.Filename())

	return p, nil
}

func (u *URL) Filename() string {
	return fmt.Sprintf("%s.httml", u.Id)
}

func (u *URL) Hostname() (string, error) {
	u_, err := url.Parse(u.Url)
	if err != nil {
		return "", errors.Stack(err)
	}

	return u_.Hostname(), nil
}
