package model

import (
	"fmt"
	"sync"
	"time"

	"github.com/samclarke/robotstxt"
	"gitlab.com/jkmn/errors"
)

type Domain struct {
	Id           string `bson:"_id"`
	Aliases      []string
	Domain       string
	Created      time.Time
	Updated      time.Time
	RobotsTxtRaw []byte
	RobotsTxtUrl string
	Blacklisted  bool
	Package      string
	rTxt         *robotstxt.RobotsTxt `bson:"-"`
	rTxtLock     sync.Mutex           `bson:"-"`
}

func (d *Domain) Url() string {
	return fmt.Sprintf("https://%s", d.Domain)
}

func (d *Domain) robotsTxt() (*robotstxt.RobotsTxt, error) {
	d.rTxtLock.Lock()
	defer d.rTxtLock.Unlock()

	if d.rTxt != nil {
		return d.rTxt, nil
	}

	var err error
	d.rTxt, err = robotstxt.Parse(string(d.RobotsTxtRaw), d.RobotsTxtUrl)
	if err != nil {
		return nil, errors.Stack(err)
	}

	return d.rTxt, nil
}

func (d *Domain) UrlPermitted(u string) (bool, error) {

	if d.RobotsTxtRaw == nil {
		return false, nil
	}

	// This robots.txt library doesn't allow much flexibility with regard to aliases.
	// If necessary, update the input URL with the domain's primary hostname.

	// u_, err := url.Parse(u)
	// if u_.Hostname() != d.Domain {
	// 	u_.Host = d.Domain
	// 	u = u_.String()
	// }

	rTxt, err := d.robotsTxt()
	if err != nil {
		return false, errors.Stack(err)
	}

	allowed, err := rTxt.IsAllowed("", u)
	if err != nil {
		return false, errors.Stack(err)
	}

	return allowed, nil
}

func (d *Domain) hasAlias(alias string) bool {
	for _, a := range d.Aliases {
		if a == alias {
			return true
		}
	}

	return false
}

func (d *Domain) AddAlias(alias string) {
	if d.hasAlias(alias) {
		return
	}

	d.Aliases = append(d.Aliases, alias)
}
