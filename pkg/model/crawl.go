package model

import "time"

type Crawl struct {
	Date         time.Time
	ResponseCode int
}
