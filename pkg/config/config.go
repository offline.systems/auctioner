package config

import (
	"os"

	"gitlab.com/offline.systems/auctioner/pkg/config/keys"

	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gitlab.com/jkmn/errors"
)

type Config interface {
	Get(key keys.Key) string
}

type config struct {
	v *viper.Viper
}

func (c *config) Get(key keys.Key) string {
	return c.v.GetString(string(key))
}

func New() Config {

	v := viper.New()

	for key, value := range defaults {
		v.SetDefault(key, value)
	}

	err := loadLocalConfig(v)
	if err != nil {
		logrus.Fatal(errors.Stack(err))
	}

	v.AutomaticEnv()

	return &config{v}
}

func Test() Config {
	v := viper.New()

	for key, value := range defaults {
		v.SetDefault(key, value)
	}

	return &config{v}
}

var defaults = map[string]string{
	keys.MongoUrl:    "mongodb://localhost/",
	keys.MongoDb:     "auctioner",
	keys.RabbitMqUrl: "amqp://localhost",
	keys.QueueUrls:   "urls",
}

func loadLocalConfig(v *viper.Viper) error {
	filename := "auctioner.env"

	f, err := os.Open(filename)
	if err != nil && os.IsNotExist(err) {
		return nil
	} else if err != nil {
		return errors.Stack(err)
	}
	f.Close()

	v.SetConfigFile("auctioner.env")
	err = v.ReadInConfig()
	if err != nil {
		return errors.Stack(err)
	}

	return nil
}
