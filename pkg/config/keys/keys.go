package keys

type Key string

const (
	MongoUrl    = "MONGO_URL"
	MongoDb     = "MONGO_DB"
	RabbitMqUrl = "RABBITMQ_URL"
	QueueUrls   = "QUEUE_URLS"
	PathPages   = "PATH_PAGES"
)
