package domcache

import (
	"log"
	"sync"
	"time"

	"gitlab.com/jkmn/errors"
	"gitlab.com/offline.systems/auctioner/pkg/bundle"
	"gitlab.com/offline.systems/auctioner/pkg/model"
	"gitlab.com/offline.systems/auctioner/pkg/store"
)

var (
	timeout = time.Minute * 5
)

type DomCache struct {
	entries map[string]cacheEntry
	lock    sync.Mutex
	s       store.Store
}

type cacheEntry struct {
	t time.Time
	d *model.Domain
}

func New(s store.Store) *DomCache {
	domCache := &DomCache{
		entries: make(map[string]cacheEntry),
		lock:    sync.Mutex{},
		s:       s,
	}

	return domCache
}

func (c *DomCache) Get(ctx bundle.B, hostname string) (*model.Domain, error) {

	dom := c.get(hostname)
	if dom != nil {
		return dom, nil
	}

	dom, err := c.s.GetDomain(ctx, hostname)
	if err != nil {
		return nil, errors.Stack(err)
	}

	if dom == nil {
		log.Printf("missing domain: %s", hostname)
		return nil, nil
	}

	c.set(dom)

	return dom, nil
}

func (c *DomCache) get(d string) *model.Domain {
	c.lock.Lock()
	defer c.lock.Unlock()

	if c.entries == nil {
		return nil
	}

	entry, ok := c.entries[d]
	if !ok {
		return nil
	}

	if entry.t.Before(time.Now().Add(-1 * timeout)) {
		delete(c.entries, d)
		return nil
	}

	return entry.d
}

func (c *DomCache) set(dom *model.Domain) {
	c.lock.Lock()
	defer c.lock.Unlock()

	entry := cacheEntry{
		t: time.Now(),
		d: dom,
	}

	c.entries[dom.Domain] = entry

	return
}
