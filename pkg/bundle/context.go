package bundle

import (
	"context"
	"testing"

	"gitlab.com/offline.systems/auctioner/pkg/config"

	"github.com/sirupsen/logrus"
)

type B struct {
	logrus.FieldLogger
	context.Context
	config.Config
	playerId   string
	commMethod string
}

func (b B) StartSpan(opName string) B {
	// startOptions := make([]tracer.StartSpanOption, 0)
	//
	// parentSpan, ok := tracer.SpanFromContext(b.Context)
	// if ok {
	// 	opt := tracer.ChildOf(parentSpan.Context())
	// 	startOptions = append(startOptions, opt)
	// }

	// span := tracer.StartSpan(opName, startOptions...)

	// spanCtx := span.Context()
	logger := b.FieldLogger
	// logger = logger.WithFields(logrus.Fields{
	// 	"dd.span_id":  spanCtx.SpanID(),
	// 	"dd.trace_id": spanCtx.TraceID(),
	// })

	newBundle := B{
		Context:     b.Context,
		FieldLogger: logger,
		Config:      b.Config,
	}

	return newBundle

}

func New(config config.Config) B {
	return NewWithContext(context.Background(), config)
}

func NewWithContext(ctx context.Context, config config.Config) B {
	var logger logrus.FieldLogger = logrus.StandardLogger()

	// if span, ok := tracer.SpanFromContext(ctx); ok {
	// 	spanCtx := span.Context()
	// 	logger = logger.WithFields(logrus.Fields{
	// 		"dd.trace_id": spanCtx.TraceID(),
	// 		"dd.span_id":  spanCtx.SpanID(),
	// 	})
	// }

	bundle := B{
		FieldLogger: logger,
		Context:     ctx,
		Config:      config,
	}

	return bundle
}

func NewTest(t *testing.T) B {
	logrus.SetLevel(logrus.DebugLevel)
	logrus.SetFormatter(&logrus.TextFormatter{})
	return New(config.Test())
}
