package bundle

import (
	"encoding/json"
	"log"
)

func (b B) JLog(thing interface{}) {

	// pc, file, line, _ := runtime.Caller(1)
	// func_ := runtime.FuncForPC(pc)
	// funcChunks := strings.Split(func_.Name(), "/")
	// funcName := funcChunks[len(funcChunks)-1]
	//
	// log.Printf("%s:%d\t(%s)\n", file, line, funcName)

	output, err := json.MarshalIndent(thing, "", "\t")
	if err != nil {
		b.Error(err)
	}
	log.Printf("%s", output)
}
