package bundle

func (b B) PlayerId() string {
	return b.playerId
}

func (b *B) SetPlayerId(playerId string) {
	b.playerId = playerId
	b.FieldLogger = b.FieldLogger.WithField("player_id", playerId)
}

func (b B) CommMethod() string {
	return b.commMethod
}

func (b *B) SetCommMethod(commMethod string) {
	b.FieldLogger = b.FieldLogger.WithField("comm_method", commMethod)
	b.commMethod = commMethod
}
