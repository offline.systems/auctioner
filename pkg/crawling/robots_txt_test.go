package crawling_test

import (
	"io/ioutil"
	"log"
	"testing"

	"github.com/samclarke/robotstxt"
	"github.com/stretchr/testify/assert"
)

func TestSamClarke(t *testing.T) {
	a := assert.New(t)
	b, err := ioutil.ReadFile("ebay_robots.txt")
	a.NoError(err)

	url := "https://www.ebay.com/robots.txt"

	rtxt, err := robotstxt.Parse(string(b), url)
	a.NoError(err)

	log.Print("Host:", rtxt.Host())
	for _, sm := range rtxt.Sitemaps() {
		log.Print("SiteMap:", sm)
	}
	log.Print("CrawlDelay:", rtxt.CrawlDelay("UserAgent 1.0"))

}
