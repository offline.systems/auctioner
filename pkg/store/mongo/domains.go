package mongo

import (
	"time"

	"gitlab.com/jkmn/errors"
	"gitlab.com/offline.systems/auctioner/pkg/bundle"
	"gitlab.com/offline.systems/auctioner/pkg/model"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

func (s *MongoStore) domains(ctx bundle.B) (*mongo.Collection, error) {
	db, err := s.db(ctx)
	if err != nil {
		return nil, errors.Stack(err)
	}

	c := db.Collection("domains")

	return c, nil
}

func (s *MongoStore) InsertDomain(ctx bundle.B, domain *model.Domain) (*model.Domain, error) {
	c, err := s.domains(ctx)
	if err != nil {
		return nil, errors.Stack(err)
	}

	domain.Created = time.Now()
	domain.Updated = domain.Created
	if domain.Id == "" {
		domain.Id = primitive.NewObjectID().Hex()
	}

	_, err = c.InsertOne(ctx, domain)
	if err != nil {
		ctx.WithError(err).Error()
		return nil, errors.Stack(err)
	}

	return domain, nil
}

func (s *MongoStore) GetDomains(ctx bundle.B) ([]model.Domain, error) {
	filter := bson.D{}

	return s.getDomainsFilter(ctx, filter)
}

func (s *MongoStore) GetDomain(ctx bundle.B, hostname string) (*model.Domain, error) {

	/*
		db.domains.aggregate([
		    {
		        $match: {
		            $or:[
		                {'domain': 'www.monoprice.com'} ,
		                {'aliases': 'www.monoprice.com'}
		            ]
		        }
		    },
		    {
		        $sample: {size: 1}
		    }
		])
	*/

	pipeline := mongo.Pipeline{
		bson.D{
			{
				"$match", bson.D{
					{"$or", bson.A{
						bson.D{{"domain", hostname}},
						bson.D{{"aliases", hostname}},
					}},
				},
			},
		},
		bson.D{
			{"$sample", bson.D{
				{"size", 1},
			}},
		},
	}

	return s.getDomainAggregation(ctx, pipeline)
}

func (s *MongoStore) ReplaceDomain(ctx bundle.B, dom *model.Domain) (*model.Domain, error) {

	c, err := s.domains(ctx)
	if err != nil {
		return nil, errors.Stack(err)
	}

	filter := bson.D{
		{"_id", dom.Id},
	}

	_, err = c.ReplaceOne(ctx, filter, dom)
	if err != nil {
		ctx.WithError(err).Error()
		return nil, errors.Stack(err)
	}

	return dom, nil
}

func (s *MongoStore) getDomainsFilter(ctx bundle.B, filter bson.D) ([]model.Domain, error) {
	c, err := s.domains(ctx)
	if err != nil {
		return nil, errors.Stack(err)
	}

	cur, err := c.Find(ctx, filter)
	if err == mongo.ErrNoDocuments {
		return nil, nil
	} else if err != nil {
		ctx.WithError(err).Error()
		return nil, errors.Stack(err)
	}

	domains := make([]model.Domain, 0)

	for cur.Next(ctx) {
		d := model.Domain{}
		err = cur.Decode(&d)
		if err != nil {
			ctx.WithError(err).Error()
			return nil, errors.Stack(err)
		}
		domains = append(domains, d)
	}

	return domains, nil
}

func (s *MongoStore) getDomainFilter(ctx bundle.B, filter bson.D) (*model.Domain, error) {
	c, err := s.domains(ctx)
	if err != nil {
		return nil, errors.Stack(err)
	}

	result := c.FindOne(ctx, filter)
	if result.Err() == mongo.ErrNoDocuments {
		return nil, nil
	} else if result.Err() != nil {
		ctx.WithError(err).Error()
		return nil, errors.Stack(result.Err())
	}

	d := &model.Domain{}
	err = result.Decode(d)
	if err != nil {
		ctx.WithError(err).Error()
		return nil, errors.Stack(err)
	}

	return d, nil
}

func (s *MongoStore) getDomainAggregation(ctx bundle.B, pipeline mongo.Pipeline) (*model.Domain, error) {
	c, err := s.domains(ctx)
	if err != nil {
		return nil, errors.Stack(err)
	}

	cursor, err := c.Aggregate(ctx, pipeline)
	if err != nil {
		ctx.WithError(err).Error()
		return nil, errors.Stack(err)
	}

	defer cursor.Close(ctx)

	var dom *model.Domain
	for cursor.Next(ctx) {
		dom = &model.Domain{}
		err = cursor.Decode(dom)
		if err != nil {
			ctx.WithError(err).Error()
			return nil, errors.Stack(err)
		}
	}

	return dom, nil
}
