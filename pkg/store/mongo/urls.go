package mongo

import (
	"time"

	"gitlab.com/jkmn/errors"
	"gitlab.com/offline.systems/auctioner/pkg/bundle"
	"gitlab.com/offline.systems/auctioner/pkg/model"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func (s *MongoStore) urls(ctx bundle.B) (*mongo.Collection, error) {
	db, err := s.db(ctx)
	if err != nil {
		return nil, errors.Stack(err)
	}

	c := db.Collection("urls")

	return c, nil
}

func (s *MongoStore) GetUrlsScannedBefore(ctx bundle.B, t time.Time) ([]model.URL, error) {

	/*
		db.urls.aggregate([
		    {
		        $match: {
		            'scanned':{
		                $lte: new Date("2022-01-19T22:00:00.000Z")
		            }
		        }
		    },
		    {
		        $sample: {size: 1}
		    }
		])
	*/
	pipeline := mongo.Pipeline{
		bson.D{
			{"$match", bson.D{
				{"crawled", bson.D{{"$lte", t}}},
			}},
		},
		bson.D{
			{"$sample", bson.D{
				{"size", 100},
			}},
		},
	}

	return s.getUrlsAggregation(ctx, pipeline)
}

func (s *MongoStore) GetUrlByUrl(ctx bundle.B, u string) (*model.URL, error) {
	filter := bson.D{
		{"url", u},
	}

	return s.getUrlFilter(ctx, filter)
}

func (s *MongoStore) InsertUrl(ctx bundle.B, u *model.URL) (*model.URL, error) {
	// TODO implement me
	panic("implement me")
}

func (s *MongoStore) UpsertUrl(ctx bundle.B, u *model.URL) (*model.URL, error) {
	c, err := s.urls(ctx)
	if err != nil {
		return nil, errors.Stack(err)
	}

	if u.Id == "" {
		u.Id = primitive.NewObjectID().Hex()
		u.Created = time.Now()
	}

	filter := bson.D{
		{"url", u.Url},
	}

	update := bson.D{
		{"$set", bson.D{
			{"robotspermitted", u.RobotsPermitted},
			{"crawled", u.Crawled},
			{"laststatus", u.LastStatus},
		}},
		{"$setOnInsert", bson.D{
			{"_id", u.Id},
			{"url", u.Url},
			{"created", u.Created},
		}},
	}

	opts := options.FindOneAndUpdate()
	opts.SetUpsert(true)
	opts.SetReturnDocument(options.After)

	result := c.FindOneAndUpdate(ctx, filter, update, opts)
	if result.Err() != nil {
		ctx.WithError(result.Err()).Error()
		return nil, errors.Stack(result.Err())
	}

	out := &model.URL{}
	err = result.Decode(out)
	if err != nil {
		return nil, errors.Stack(err)
	}

	return out, nil
}

func (s *MongoStore) UpdateUrlCrawled(ctx bundle.B, id string) error {
	c, err := s.urls(ctx)
	if err != nil {
		return errors.Stack(err)
	}

	filter := bson.D{
		{"_id", id},
	}

	update := bson.D{
		{"$set", bson.D{
			{"crawled", time.Now()},
		}},
	}

	_, err = c.UpdateOne(ctx, filter, update)
	if err != nil {
		ctx.WithError(err).Error()
		return errors.Stack(err)
	}

	return nil
}

func (s *MongoStore) UpdateUrlDownloaded(ctx bundle.B, id string, status int) error {
	c, err := s.urls(ctx)
	if err != nil {
		return errors.Stack(err)
	}

	filter := bson.D{
		{"_id", id},
	}

	update := bson.D{
		{"$set", bson.D{
			{"downloaded", time.Now()},
			{"laststatus", status},
		}},
	}

	_, err = c.UpdateOne(ctx, filter, update)
	if err != nil {
		ctx.WithError(err).Error()
		return errors.Stack(err)
	}

	return nil
}

func (s *MongoStore) getUrlsFilter(ctx bundle.B, filter bson.D) ([]model.URL, error) {
	c, err := s.urls(ctx)
	if err != nil {
		return nil, errors.Stack(err)
	}

	cur, err := c.Find(ctx, filter)
	if err == mongo.ErrNoDocuments {
		return nil, nil
	} else if err != nil {
		ctx.WithError(err).Error()
		return nil, errors.Stack(err)
	}

	urls := make([]model.URL, 0)

	for cur.Next(ctx) {
		u := model.URL{}
		err = cur.Decode(&u)
		if err != nil {
			ctx.WithError(err).Error()
			return nil, errors.Stack(err)
		}
		urls = append(urls, u)
	}

	return urls, nil

}

func (s *MongoStore) getUrlFilter(ctx bundle.B, filter bson.D) (*model.URL, error) {
	c, err := s.urls(ctx)
	if err != nil {
		return nil, errors.Stack(err)
	}

	result := c.FindOne(ctx, filter)
	if result.Err() == mongo.ErrNoDocuments {
		return nil, nil
	} else if result.Err() != nil {
		ctx.WithError(err).Error()
		return nil, errors.Stack(result.Err())
	}

	u := &model.URL{}
	err = result.Decode(u)
	if err != nil {
		ctx.WithError(err).Error()
		return nil, errors.Stack(err)
	}

	return u, nil
}

func (s *MongoStore) getUrlsAggregation(ctx bundle.B, pipeline mongo.Pipeline) ([]model.URL, error) {
	c, err := s.urls(ctx)
	if err != nil {
		return nil, errors.Stack(err)
	}

	cursor, err := c.Aggregate(ctx, pipeline)
	if err != nil {
		ctx.WithError(err).Error()
		return nil, errors.Stack(err)
	}

	z := make([]model.URL, 0)

	defer cursor.Close(ctx)

	for cursor.Next(ctx) {
		x := model.URL{}
		err = cursor.Decode(&x)
		if err != nil {
			ctx.WithError(err).Error()
			return nil, errors.Stack(err)
		}
		z = append(z, x)
	}

	return z, nil
}
