package mongo

import (
	"sync"

	"gitlab.com/jkmn/errors"
	"gitlab.com/offline.systems/auctioner/pkg/bundle"
	"gitlab.com/offline.systems/auctioner/pkg/config/keys"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func New() *MongoStore {
	return &MongoStore{}
}

type MongoStore struct {
	_client     *mongo.Client
	_clientLock sync.Mutex
	_db         *mongo.Database
	_dbLock     sync.Mutex
}

func (s *MongoStore) client(ctx bundle.B) (*mongo.Client, error) {
	if s._client != nil {
		return s._client, nil
	}

	s._clientLock.Lock()
	defer s._clientLock.Unlock()

	uri := ctx.Get(keys.MongoUrl)

	client, err := mongo.NewClient(options.Client().ApplyURI(uri))
	if err != nil {
		ctx.WithError(err).Error()
		return nil, errors.Stack(err)
	}

	err = client.Connect(ctx)
	if err != nil {
		ctx.WithError(err).Error()
		return nil, errors.Stack(err)
	}

	s._client = client
	return s._client, nil
}

func (s *MongoStore) db(ctx bundle.B) (*mongo.Database, error) {
	if s._db != nil {
		return s._db, nil
	}

	s._dbLock.Lock()
	defer s._dbLock.Unlock()

	cl, err := s.client(ctx)
	if err != nil {
		return nil, errors.Stack(err)
	}

	dbName := ctx.Get(keys.MongoDb)
	if dbName == "" {
		return nil, errors.Newf("config value %s not set", keys.MongoDb)
	}
	s._db = cl.Database(dbName)

	return s._db, nil
}

func boolPtr(t bool) *bool {
	return &t
}
func int8Ptr(i int8) *int8 {
	return &i
}
