package store

import (
	"time"

	"gitlab.com/offline.systems/auctioner/pkg/bundle"
	"gitlab.com/offline.systems/auctioner/pkg/model"
)

type Store interface {
	// Domains
	GetDomain(ctx bundle.B, hostname string) (*model.Domain, error)
	GetDomains(ctx bundle.B) ([]model.Domain, error)
	InsertDomain(ctx bundle.B, domain *model.Domain) (*model.Domain, error)
	ReplaceDomain(ctx bundle.B, dom *model.Domain) (*model.Domain, error)

	// Urls
	GetUrlsScannedBefore(ctx bundle.B, t time.Time) ([]model.URL, error)
	InsertUrl(ctx bundle.B, u *model.URL) (*model.URL, error)
	UpdateUrlCrawled(ctx bundle.B, id string) error
	UpdateUrlDownloaded(ctx bundle.B, id string, status int) error
	UpsertUrl(ctx bundle.B, u *model.URL) (*model.URL, error)
}
