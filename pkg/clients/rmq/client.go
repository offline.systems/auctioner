package rmq

import (
	"bytes"
	"encoding/json"
	stdErrors "errors"

	amqp "github.com/rabbitmq/amqp091-go"
	"gitlab.com/jkmn/errors"
	"gitlab.com/offline.systems/auctioner/pkg/bundle"
	"gitlab.com/offline.systems/auctioner/pkg/config/keys"
	"gitlab.com/offline.systems/auctioner/pkg/model"
)

var (
	ErrNoListeners = stdErrors.New("no listeners")
)

type Client struct {
	*amqp.Connection
}

func New(ctx bundle.B) (*Client, error) {
	uri := ctx.Config.Get(keys.RabbitMqUrl)

	connection, err := amqp.Dial(uri)
	if err != nil {
		return nil, errors.Stack(err)
	}

	c := &Client{
		Connection: connection,
	}

	return c, nil
}

func (c *Client) Consume(queueName string) (<-chan amqp.Delivery, error) {
	channel, err := c.Channel()
	if err != nil {
		return nil, errors.Stack(err)
	}

	args := amqp.Table{
		// "x-message-ttl": 60000,
	}

	_, err = channel.QueueDeclare(queueName, false, true, false, false, args)
	if err != nil {
		channel.Close()
		return nil, errors.Stack(err)
	}

	deliveries, err := channel.Consume(queueName, "", false, false, false, false, nil)
	if err != nil {
		return nil, errors.Stack(err)
	}

	return deliveries, nil
}

func (c *Client) SendUrls(ctx bundle.B, urls []model.URL) error {

	pubs, err := prepareUrls(urls)
	if err != nil {
		return errors.Stack(err)
	}

	qName := ctx.Get(keys.QueueUrls)
	if qName == "" {
		return errors.Newf("config value %s not set", keys.QueueUrls)
	}

	err = c.sendPubs(qName, pubs)
	if err != nil {
		return errors.Stack(err)
	}

	return nil
}

func (c *Client) sendPubs(qName string, pubs []amqp.Publishing) error {
	channel, err := c.Channel()
	if err != nil {
		return errors.Stack(err)
	}
	defer channel.Close()

	args := amqp.Table{
		// "x-message-ttl": 60000,
	}

	queue, err := channel.QueueDeclare(qName, false, true, false, false, args)
	if err != nil {
		return errors.Stack(err)
	}

	if queue.Consumers == 0 {
		return ErrNoListeners
	}

	for _, pub := range pubs {
		err = channel.Publish("", qName, false, false, pub)
		if err != nil {
			return errors.Stack(err)
		}
	}

	return nil
}

func prepareUrls(urls []model.URL) ([]amqp.Publishing, error) {

	out := make([]amqp.Publishing, 0)

	for _, u := range urls {

		// Just use JSON for now.
		buf := bytes.NewBuffer(nil)
		enc := json.NewEncoder(buf)
		enc.SetIndent("", "\t")
		err := enc.Encode(u)
		if err != nil {
			return nil, errors.Stack(err)
		}

		pub := amqp.Publishing{
			ContentType: "application/json",
			Body:        buf.Bytes(),
		}

		out = append(out, pub)
	}

	return out, nil
}
